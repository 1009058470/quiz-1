package com.twuc.webApp;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import com.twuc.webApp.classOfGame.IdAndAnswer;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class CreateGameTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void test_create_game() throws Exception {
        mockMvc.perform(post("/api/games")).andExpect(status().isCreated());
    }

    @Test
    void test_create_game_headers() throws Exception {
        mockMvc.perform(post("/api/games/1"))
                .andExpect(header().string("Location header","/api/games/1"));
    }

    @Test
    void test_create_game_headers2() throws Exception {
        mockMvc.perform(post("/api/games/2"))
                .andExpect(header().string("Location header","/api/games/2"));
    }


    @Test
    void test_response_of_the_game() throws Exception {
        mockMvc.perform(post("/api/games/1"));
        mockMvc.perform(get("/api/games/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"));
               // .andExpect(content().string("{\"id\":1,\"answer\":1234}")); // 这个是写死的答案
    }

    @Test
    void test_if_id_one() throws Exception {
        mockMvc.perform(post("/api/games/2"));
        mockMvc.perform(get("/api/games/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"));
                //.andExpect(content().string("{\"id\":2,\"answer\":1234}")); // 这个是写死的答案
    }

    @Test
    void test_donot_have_gameId_the_response_of_404() throws Exception {
        mockMvc.perform(get("/api/games/3"))
                .andExpect(status().isNotFound());
    }

    @Test
    void test_patch_response() throws Exception {
        mockMvc.perform(post("/api/games/4"));
        mockMvc.perform(patch("/api/games/4").contentType("application/json").content("{\"userAnswer\":1325}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"));
                //.andExpect(content().string("{\"hint\":\"1A2B\",\"correct\":false}"));
    }


}
