package com.twuc.webApp.utils;

import java.util.Random;

public class MakeA4Number {
    // 随机生成一个没有重复数字的数
    public static int random() {
        Random random = new Random(System.currentTimeMillis());
        int number = 0;
        boolean ok = true;
        do {
            ok = true;
            number = random.nextInt(9000) + 1000;
            int[] digits = {
                    number / 1000 % 10,
                    number / 100 % 10,
                    number / 10 % 10,
                    number % 10
            };
            for (int i = 0; i < 4 && ok; i++) {
                for (int j = i + 1; j < 4; j++) {
                    if (digits[i] == digits[j]) {
                        ok = false;
                        break;
                    }
                }
            }
        } while (!ok);
        return number;
    }
}
