package com.twuc.webApp;

import com.twuc.webApp.utils.MakeA4Number;

public class Game {
    private int id;
    private int answer = 1234;


    public Game(int id){
        this.id = id;
        this.answer = MakeA4Number.random();
    }

    public int getAnswer() {
        return answer;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
