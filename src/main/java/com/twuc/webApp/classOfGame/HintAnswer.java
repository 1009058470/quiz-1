package com.twuc.webApp.classOfGame;

public class HintAnswer {
    private String hint;
    private boolean correct;

    public HintAnswer(String hint, boolean correct){
        this.hint = hint;
        this.correct = correct;
        //hint = "1A2B";
        //correct = false;
    }

    public String getHint() {
        return hint;
    }

    public boolean getCorrect() {
        return correct;
    }

    public static String calcHint(int userAns, int realAns){
        String actualAnswer = ""+realAns;
        String exceptAnswer = ""+userAns;
        int correct = 0;
        for (int startIndex = 0; startIndex < 4; startIndex ++) {
            if (actualAnswer.charAt(startIndex) == exceptAnswer.charAt(startIndex)) {
                correct++;
            }
        }
        int contains = 0;
        for (int startIndex = 0; startIndex < 4; startIndex ++) {
            for (int exceptIndex = 0; exceptIndex < 4; exceptIndex++) {
                if (actualAnswer.charAt(startIndex) == exceptAnswer.charAt(exceptIndex)) {
                    if (startIndex != exceptIndex) {
                        contains++;
                    }
                }
            }

        }
        return String.format("%sA%sB", correct, contains);
    }

    public static boolean calcCorrect(String hint) {
        if (hint.equals("4A0B"))
            return true;
        return false;
    }
}
