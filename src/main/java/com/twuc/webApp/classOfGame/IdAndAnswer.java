package com.twuc.webApp.classOfGame;

public class IdAndAnswer {
    int id;
    int answer;

    public IdAndAnswer(int id, int ans){
        this.id = id;
        this.answer = ans;
    }

    /*json 在解析的时候要用getter和默认的构造方法*/

    public int getId() {
        return id;
    }

    public int getAnswer() {
        return answer;
    }
}
