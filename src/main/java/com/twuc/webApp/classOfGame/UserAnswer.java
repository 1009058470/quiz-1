package com.twuc.webApp.classOfGame;

public class UserAnswer {
    private int id;
    private int userAnswer;

    public UserAnswer(int id,int userAnswer){
        this.id=id;
        this.userAnswer=userAnswer;
    }


    public UserAnswer(int userAnswer) {
        this.userAnswer = userAnswer;
    }

    public int getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(int userAnswer) {
        this.userAnswer = userAnswer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
