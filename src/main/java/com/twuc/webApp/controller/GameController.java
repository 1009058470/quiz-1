package com.twuc.webApp.controller;


import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.twuc.webApp.Game;
import com.twuc.webApp.classOfGame.HintAnswer;
import com.twuc.webApp.classOfGame.IdAndAnswer;
import com.twuc.webApp.classOfGame.UserAnswer;
import com.twuc.webApp.utils.MakeA4Number;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


@RestController
@RequestMapping("/api/games")
public class GameController {
    Set<Integer> SetGameId = new TreeSet<>();
    Map<Integer, Integer> map = new HashMap<>();

    @PostMapping
    public ResponseEntity<String> createGame(){
        return ResponseEntity.status(201).build();
    }

    @PostMapping("/{gameId}")
    public ResponseEntity<IdAndAnswer> createGameAndGameId(@PathVariable String gameId){
       int id = -1;
        try {
            id = Integer.parseInt(gameId);
        }catch (RuntimeException runtimeException){
            return ResponseEntity.status(400).build();
        }
        SetGameId.add(id);
        int ans = MakeA4Number.random();
        map.put(id, ans);
        return ResponseEntity.status(201).header("Location header","/api/games/"+gameId).build();//.body(new IdAndAnswer(gameId,ans));
    }

    @GetMapping("/{gameId}")
    public ResponseEntity<IdAndAnswer> responseOfGame(@PathVariable int gameId){
        if(!SetGameId.contains(gameId)){
            return ResponseEntity.status(404).build();
        }
        int id = gameId;
        int ans = map.get(id);
        return ResponseEntity.status(200).contentType(MediaType.APPLICATION_JSON).body(new IdAndAnswer(id, ans));
    }


    @PatchMapping("/{gameId}")
    public ResponseEntity<HintAnswer> responseHint(@PathVariable int gameId, @RequestBody UserAnswer userAns){  //requestBody
        int userAns1 = userAns.getUserAnswer();
        int realAns = map.get(gameId);
        String hint = HintAnswer.calcHint(userAns1,realAns);
        boolean correct = HintAnswer.calcCorrect(hint);

        return ResponseEntity.status(200).contentType(MediaType.APPLICATION_JSON).body(new HintAnswer(hint, correct));
    }

}
